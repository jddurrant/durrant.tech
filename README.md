# Durrant.tech

Built using [Astro](https://astro.build/).

## Licensing

|Path           |License                                                                                               |
|---------------|------------------------------------------------------------------------------------------------------|
|`src/content`  |Copyright 2011-2023 Jack Durrant <[jack@durrant.tech](mailto:jack@durrant.tech)>. All rights reserved.|
|Everything else|[GNU AGPL, version 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html#license-text)                   |
