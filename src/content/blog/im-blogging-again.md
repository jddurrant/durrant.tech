---
title: I'm Blogging Again
subtitle: But, like, for real this time.
slug: im-blogging-again
---

It has been quite some time since I added anything to my blog. My last entry was a rather rushed coverage piece of CES 2014.

I was assigned to write about the event for school back then and decided I might as well put it on my blog.

## Why did I stop?

I don't have much of an interesting reason to give for why I stopped blogging. 

Primarily, I wanted to move my blog off of its WordPress back end. I felt that my WordPress site was too bloated, with too many featuers that I wasn't using.

Ignorantly, I thought rewriting the entire back end would be a ~6 month process, ending with my dream website on the initial release. For this reason, I decided not to write anything new until then.

I'll probably write about how this ended up becoming an 8 year process, resulting in several rewrites and, eventually, a rather basic website.

If nothing else, it will serve as a lesson for aspiring developers of how **not** to build a website - or any other software project, for that matter.

## Why am I back?

I've been wanting to start blogging again for quite a while.

A lot has happened in the world of technology over the last 5 years, and my opinions about technology in general have changed quite considerably.

I have a lot to say about £1000+ smartphones without headphone jacks. I'd like to share my opinion on these \"Terms Of Service\" documents that we've all agreed to at some point.

I intend to be very blunt with my opinions on this blog, and perhaps create some uncertainty around the notion that nerds like technology.

It won't all be doom and gloom, mind you. I also want to educate people through my blog.

I'd like to propose solutions to some of the problems we face with modern technology. I'd like to write about things that I, as a cynical technology enthusiast, like in the world of technology.

## The New Website

The new website is written using the Astro framework.

For the time being, I am storing this site's content alongside its code, in the [Git repo](https://gitlab.com/jddurrant/durrant.tech).

At some point, I'll probably move the content out of the repo and into a separate directory, but I'll tackle that hurdle in a future commit.

The website's source code is public and licensed under the GNU AGPL.

As for the content (including anything in `src/content`), for now, I'll reserve all legal rights to it, unless I specify otherwise.

If you want to share my content with others, feel free to send them a link. If you want to redistribute it outside of Durrant.tech, [please send me an email](mailto:jack@durrant.tech).

## What Next?

For now, I will work on improving the site's code base. It's pretty simple at the moment, but I plan to make something more feature-complete out of it.

Obviously, I also want to add new content to it - when I have the time, and when I have something good to write about.

I have a full time job, so progress may be a little slow. That being said, feel free to [annoy me](mailto:jack@durrant.tech) if I seem to be taking an extended break on the project; I probably need the motivation.
